class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def json_api_response(resource, options = {})
    options[:adapter] ||= :json_api
    options[:key_transform] ||= :camel_lower
    #options[:namespace] ||= API
    ActiveModelSerializers::SerializableResource.new(resource, options)
  end

  def json_api_resp(resource, options = {})
    #binding.pry
    resource[:meta] = options[:meta] if options.has_key?(:meta)
    resource
  end
end
