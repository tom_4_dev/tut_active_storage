class Api::CatsController < ApplicationController
  def index
    cats = Cat.all.find_json_array(:id, :name, :weight, :color, :created_at)
    render status: :ok, json: json_api_resp(cats, meta: { meta_info: "this is meta info" })
  end

  def show
    cat = Cat.find_json_record(params[:id], :name, :weight, :color, :created_at)
    render status: :ok, json: cat
  end

  def serialized
    cats = Cat.where("id > ?", 0).includes(:cat_houses)

    render status: :ok, json: json_api_response(cats, include: [], meta: { meta_info: "this is meta info" }).as_json
  end
end
