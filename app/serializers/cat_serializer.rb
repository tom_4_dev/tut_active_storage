class CatSerializer < ActiveModel::Serializer
  attributes :id, :name, :weight, :created_at
  has_many :cat_houses, serializer: CatHouseSerializer
end
