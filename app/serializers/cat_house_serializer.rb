class CatHouseSerializer < ActiveModel::Serializer
  attributes :id, :name, :zip_code
end
