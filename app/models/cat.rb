class Cat < ApplicationRecord
  ###include APIsJson
  validates :name, presence: true
  has_one_attached :picture
  has_many :cat_houses
end
