# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

cat = Cat.find_or_create_by(name: "Pussy")

cat.cat_houses.create(name: "House 1", address_1: "sjdhdsdj", zip_code: "700157")
cat.cat_houses.create(name: "House 2", address_1: "sjdhddsd sdj", zip_code: "700059")
cat.cat_houses.create(name: "House 3", address_1: "dja sdjfhd sjdhdsdj", zip_code: "700001")
