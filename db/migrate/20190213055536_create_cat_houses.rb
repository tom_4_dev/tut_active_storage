class CreateCatHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :cat_houses do |t|
      t.integer :cat_id, null: false
      t.string :name
      t.text :address_1
      t.string :country
      t.string :zip_code

      t.timestamps
    end

    add_foreign_key :cat_houses, :cats
  end
end
