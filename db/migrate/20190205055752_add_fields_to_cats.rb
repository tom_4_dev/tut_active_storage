class AddFieldsToCats < ActiveRecord::Migration[5.2]
  def change
    add_column :cats, :color, :string
    add_column :cats, :weight, :decimal
    add_column :cats, :height, :decimal
  end
end
